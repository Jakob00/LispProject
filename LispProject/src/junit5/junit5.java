package junit5;


import org.junit.Test;

import elements.LispElements;
import lisp.MyLisp;

public class junit5
{
    interface ReturningLispCode
    {
        LispElements run(MyLisp myLisp);
    }

    interface LispCode
    {
        void run(MyLisp myLisp);
    }

    public void runLisp(LispCode code)
    {
        code.run(new MyLisp());
    }

    public LispElements runLisp(ReturningLispCode code)
    {
        return code.run(new MyLisp());
    }

    public String runLisp(String code)
    {
        return new MyLisp().run(code);
    }

    public LispElements runLispAndReturnPart(String code)
    {
        return new MyLisp().runAndReturnPart(code);
    }

    
    @Test
    public void testComments()
    {

    }

}
