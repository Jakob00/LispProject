package lambda;

/**
 * Interface till lambda funktioner
 *
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public interface Lambda1<A, B>
{
    /**
    * En metod för exekvering
    *
    * @param a av A
    * @return ett objekt av B
    *
    */
    @SuppressWarnings("unchecked")
	public B exec( A... a);
    
    /**
    * Interface till lambda funktioner
    *
    * @author Jakob Nygård 19jany01
    * @author Joel Johansson 19jojo15
    * @version 2020-01-11
    *
    */
    interface Lambda2<A, B>
    {
        /**
        * En metod för exekvering
        *
        * @param a av A
        * @return ett objekt av B
        *
        */
        public B exec(A a);
    }
    
    /**
    * Interface till lambda funktioner
    *
    * @author Jakob Nygård 19jany01
    * @author Joel Johansson 19jojo15
    * @version 2020-01-11
    *
    */
    interface Lambda3<A, B, C>
    {
        /**
        * En metod för exekvering
        *
        * @param a av A
        * @param b av B
        * @return ett objekt av C
        *
        */
        public C exec(A a, B b);
    }
    
    /**
    * Interface till lambda funktioner
    *
    * @author Jakob Nygård 19jany01
    * @author Joel Johansson 19jojo15
    * @version 2020-01-11
    *
    */
    interface Lambda4<A, B>
    {
        /**
        * En metod för exekvering
        *
        * @param a av A
        * @param b av A
        * @return ett objekt av B
        *
        */
        public B exec(A a, A b);
    }

}

