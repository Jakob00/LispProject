package lisp;

import java.util.Scanner;


/**
 * Klassen som kör lisp-interpretatorn
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class ReadEvalPrintLoop
{
    /**
    * Main metoden som loopar lisp-interpretatorn 
    *
    * @param args argument från kommandoraden
    *
    */
    public static void main(String[] args) throws Exception
    {
        System.out.println("LISP INTEPRETER");
        System.out.print("Input code: ");
        MyLisp myLisp = new MyLisp();
        
        try (Scanner s = new Scanner(System.in))
        {
            String string;
            while (!(string = s.nextLine()).equals("stop"))
            {
                try
                {
                    System.out.println(myLisp.run(string));
                }
                catch (Exception e)
                {
                    System.out.println(e.getClass().getSimpleName() + e.getMessage());
                }
                System.out.print(": ");
            }
        }
    }
}
