package lisp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import elements.Cells;
import elements.LispElements;
import elements.Element;
import elements.LambdaElement;
import elements.Values;
import elements.StringElement;
import elements.SymbolElement;
import elements.ValueLisp;
import lambda.Lambda1;
import other.Enviroment;
import other.Scannable;
import other.StackForChar;

/**
 * Klassen som sammlar alla andra klasser, här läggs alla funktioner in i ett Environment och inskickade instruktioner kontrolleras
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class MyLisp
{
    private static final LispElements t = new LispElements(){
        @Override
        public String string(){
            return "t";
        }
    };
    
    public static final Cells NIL = new Cells(){
        @Override
        public String cellListToString(){
            return "NIL";
        }
    };

    
        
        private Element l4(Element[] partList, Lambda1.Lambda4<ValueLisp, ValueLisp> i)
        {
            try
            {
                if(partList.length < 2)
                    throw new RuntimeException();

                ValueLisp answer;
                answer = i.exec(((Values) partList[0].getE()).t,
                		((Values) partList[1].getE()).t) ;
                for(int j = 2; j < partList.length;j++) {
                	
                	answer = i.exec(answer, ((Values) partList[j].getE()).t);
                	
                }
                    

                return new Element(new Values(answer));
            }
            catch (RuntimeException e){
                throw new RuntimeException("");
            }
        }

        
        private Element l2(Element[] partList, Lambda1.Lambda2<ValueLisp, ValueLisp> i)
        {
            try
            {
                if(partList.length != 1)
                    throw new RuntimeException();
                return new Element(new Values(i.exec(((Values) partList[0].getE()).t)));
            }
            catch (RuntimeException e)
            {
                throw new RuntimeException("");
            }
        }

        
        private boolean check(Element[] args, Scannable... x)
        {
            if(args.length != x.length) {
            	
            	 return false;
            }
               

            for(int i = 0; i < x.length; i++) {
            	
            	if(!x[i].scan(args[i])) {
            		
            		  return false;
            	}
            	
            }
                

            return true;
        }
        
      

    
         public Enviroment publicEnvir = new Enviroment(){
            Scannable isAny = (p) -> true;
            public Element boolNumberOp(Element[] partList, Lambda1.Lambda4<ValueLisp, Boolean> i)
            {
                try
                {
                    if(partList.length < 2)
                        throw new RuntimeException();

                    boolean a;
                    ValueLisp last;
                    a = i.exec(((Values) partList[0].getE()).t, last = ((Values) partList[1].getE()).t);
                    for(int j = 2; j < partList.length; j++)
                        a = a && i.exec(last, last = ((Values) partList[j].getE()).t);

                    return new Element(a ? t : NIL);
                }
                catch (RuntimeException e)
                {
                    throw new RuntimeException("");
                }
            }

        private void lispOp(String s, Lambda1<Element, Element> l)
        {
            envsetter(s, new Element(new LambdaElement(l)));
        }

        {
        	hMenv.put("pi", new Element(new Values(Math.PI)));
        	hMenv.put("nil", new Element(NIL));
        	hMenv.put("e", new Element(new Values(Math.E)));
        	hMenv.put("t", new Element(t));
             
        	 lispOp("ceil", (args) -> l2(args, (a) -> a.lispceil()));
        	 
             lispOp("floor", (args) -> l2(args, (a) -> a.lispfloor()));
             
             lispOp("round", (args) -> l2(args, (a) -> a.lispround()));
             
             lispOp("sqrt", (args) -> l2(args, (a) -> a.lispsqrt()));
             
            lispOp(">", (args) -> boolNumberOp(args, (a, b) -> a.compareTo(b) > 0));
            
            lispOp("<", (args) -> boolNumberOp(args, (a, b) -> a.compareTo(b) < 0));
            
            lispOp(">=", (args) -> boolNumberOp(args, (a, b) -> a.compareTo(b) >= 0));
            
            lispOp("<=", (args) -> boolNumberOp(args, (a, b) -> a.compareTo(b) <= 0));
            
            lispOp("=", (args) -> boolNumberOp(args, (a, b) -> a.compareTo(b) == 0));
            
            lispOp("eq", (args) -> check(args, isAny, isAny) ? new Element(args[0] == args[1] ? t : NIL) : null);
            
            lispOp("max", (args) -> l4(args, (a, b) -> a.lispmax(b)));
            
            lispOp("min", (args) -> l4(args, (a, b) -> a.lispmin(b)));
            
            lispOp("+", (args) -> l4(args, (a, b) -> a.lispPlus(b)));
            
            lispOp("-", (args) -> l4(args, (a, b) -> a.lispMinus(b)));
            
            lispOp("*", (args) -> l4(args, (a, b) -> a.lispTimes(b)));
            
            lispOp("/", (args) -> l4(args, (a, b) -> a.lispObelus(b)));
            
            lispOp("asin", (args) -> l2(args, (a) -> a.lispasin()));
            
            lispOp("acos", (args) -> l2(args, (a) -> a.lispacos()));
            
            lispOp("atan", (args) -> l2(args, (a) -> a.lispatan()));
            
            lispOp("abs", (args) -> l2(args, (a) -> a.lispabs()));
            
            lispOp("sin", (args) -> l2(args, (a) -> a.lispsin()));
            
            lispOp("cos", (args) -> l2(args, (a) -> a.lispcos()));
            
            lispOp("tan", (args) -> l2(args, (a) -> a.lisptan()));

            lispOp("exp", (args) -> check(args, isAny, isAny) ? l4(args, (a, b) -> a.lispexp(b)) : null);

           
        }
    };

    private Element stackToPart(StackForChar stack)
    {
        try
        {
            stack.clean();
        }
        catch (RuntimeException e)
        {
            throw new RuntimeException("')' expected at end");
        }

        char c = stack.pop();
        
        if(c == '\'')
        {
            return new Element(new Cells(new Element(new SymbolElement("quote")), new Element(new Cells(stackToPart(stack), new Element(NIL)))));
        }
        
        if(c == ')') {
        	throw new RuntimeException("Unexpected ')'");
        }
            
        if(c == '('){
            stack.clean();

            if(stack.next(')'))
            {
                stack.pop();
                return new Element(NIL);
            }

            List<Element> partList = new ArrayList<Element>();
            partList.add(stackToPart(stack));

            do
            {
                stack.clean();

                if(stack.next(')')) {
                    stack.pop();
                    return new Element(new Cells(partList.toArray(new Element[0])));
                }

                if(stack.next('.')){
                    stack.pop();
                    if(stack.noCharAtNext() || stack.next('(')){
                        Element part = stackToPart(stack);
                        stack.clean();
                        if(!stack.next(')')){
                            throw new RuntimeException("Invalid cons");
                        }
                        stack.pop();
                        return new Element(new Cells(partList.toArray(new Element[0]), part));
                    }
                    else{
                        stack.unPop();
                        partList.add(stackToPart(stack));
                    }
                }else
                    partList.add(stackToPart(stack));
            }while (true);
        }else{
            StringBuilder sB = new StringBuilder();
            sB.append(c);
            char zero = 0;
            if(c == '"')
                zero = c;
            boolean closed = false;
            boolean isString = zero != 0;
           

            while (stack.size() != 0 && (zero != 0 || !stack.atLast(stack.peak()))){
                sB.append(stack.pop());
                if(zero != 0 && (zero == stack.peak() && '\\' != stack.peak(-1)))
                {
                	closed = true;
                    sB.append(stack.pop());
                    break;
                }
            }

            if(!closed && isString) {
            	throw new RuntimeException("");
            }
                
            String s = sB.toString();

            if(isString) {
            	   return new Element(new StringElement(s));
            }
            try{
                return new Element(new Values(new ValueLisp(s)));
            }
            catch (RuntimeException error)
            {
                return new Element(new SymbolElement(s));
            }
        }
    }
    

    private Element partMethod(Element part)
    {
        return partMethod(part, publicEnvir);
    }

    HashMap<String, Integer> hM = new HashMap<String, Integer>()
    {
        /**
		 * 
		 */
		private static final long serialVersionUID = 8812838769740889730L;

		{
            put("first", 0);
            
            put("second", 1);
            
            put("third", 2);
            
            put("fourth", 3);
            
            put("fifth", 4);
            
            put("sixth", 5);
            
            put("seventh", 6);
            
            put("eighth", 7);
            
            put("ninth", 8);
            
            put("tenth", 9);
        }
    };

    private Element partMethod(Element partcontainer, Enviroment enviroment)
    {
        LispElements p2 = partcontainer.getE();
        
        if(p2 instanceof SymbolElement)
        {
            return enviroment.get(((SymbolElement) p2).s);
        }
        
        if(!(p2 instanceof Cells)) 
        {
            return partcontainer;
        }

        
        if(p2.equals(NIL)) {
        	return partcontainer;
        }

       

        
        Element[] partList2 = ((Cells) p2).array();
        LispElements[] parts = Element.asParts(partList2);

        if(parts[0] instanceof SymbolElement)
        {
            String s = ((SymbolElement) parts[0]).s;
            Element[] contain = partList2.length == 0 ? new Element[0] 
            		: Arrays.copyOfRange(partList2, 1, partList2.length);
            LispElements[] part = Element.asParts(contain);

            if(s.equals("nil"))
            {
                if(part.length != 0) {
                	throw new RuntimeException("error, nil should be empty () or false");
                }
                    
                return new Element(NIL);
            }

            if(s.equals("quote"))
            {
                if(part.length != 1) {
                	throw new RuntimeException("error, quote should have only one t");
                }
                    
                return contain[0];
            }

            if(s.equals("print"))
            {
                Element a = new Element(NIL);
                for(Element contain2 : contain) {
                	System.out.println((a = partMethod(contain2, enviroment)).asString());
                }
                    

                return a;
            }

            if(s.equals("text"))
            {
                if(part.length != 1) {
                	throw new RuntimeException("error, text should have one t");
                }
                    

                return new Element(new StringElement(partMethod(contain[0],
                		enviroment).getE().string()));
            }

            if(s.equals("if"))
            {
                if(part.length == 2 || part.length == 3)
                {
                    Element tOrF = partMethod(contain[0], enviroment);

                    if(tOrF.getE().equals(NIL))
                    {
                        if(part.length == 3) 
                        {
                            return partMethod(contain[2], enviroment);
                        }
                        else
                            return tOrF;
                    }
                    else
                        return partMethod(contain[1], enviroment);
                }
                else
                    throw new RuntimeException();
            }

            if(s.equals("cond"))
            {
                if(part.length == 0) {
                	return new Element(NIL);	
                }
                    

                for(LispElements p : part) {
                	 if(!(p instanceof Cells)) {
                		 throw new RuntimeException("error, should be cons as argument");
                	 }     
                }
                Element a = new Element(NIL);
                for(LispElements p1 : part)
                {
                    if(p1.equals(NIL)) {
                        continue;
                    }
                    Element[] containers = ((Cells) p1).array();

                    a = partMethod(containers[0], enviroment);
                    if(!a.getE().equals(NIL))
                    {
                        for(int j = 1; j < containers.length; j++) {
                        	  a = partMethod(containers[j], enviroment);
                        }         
                        return a;
                    }
                }
                return a;
            }

            if(s.equals("let"))
            {
                if(part.length == 0) 
                {
                	throw new RuntimeException("");
                }
                    

                if(!(part[0] instanceof Cells))
                {
                	throw new RuntimeException("error, cons should be first");	
                }                                 
                Element[] cont3 = ((Cells) part[0]).array();
                LispElements[] part3 = Element.asParts(cont3);
                Enviroment envir = new Enviroment(enviroment);
                HashMap<String, Element> hM3 = new HashMap<String, Element>();

                for(int j = 0; j < part3.length; j++) {
                    LispElements p = part3[j];
                    if(p instanceof SymbolElement) 
                    {
                    	hM3.put(((SymbolElement) p).s, new Element(NIL));
                    }else
                        if(p instanceof Cells && ((Cells) p).element1.getE() instanceof SymbolElement) 
                        {
                            hM3.put(((SymbolElement) ((Cells) p).element1.getE()).s, partMethod(((Cells) ((Cells) p).element2.getE()).element1, envir));

                        }else
                            throw new RuntimeException("//todo"); // TODO fix exception
                }

                for(Entry<String, Element> entry : hM3.entrySet()){
                    envir.envsetter(entry.getKey(), entry.getValue());
                }

                Element cont4 = new Element(NIL);
                for(int i = 1; i < part.length; i++){
                    cont4 = partMethod(contain[i], envir);
                }

                return cont4;
            }
            if(s.equals("let*"))
            {
                if(part.length == 0) 
                {
                	  throw new RuntimeException("//todo");
                }
                  

                if(!(part[0] instanceof Cells)) 
                {
                	throw new RuntimeException("// TODO");
                }
                   

                

                Element[] cont4 = ((Cells) part[0]).array();
                LispElements[] params = Element.asParts(cont4);
                Enviroment envir = new Enviroment(enviroment);
                for(int j = 0; j < params.length; j++)
                {
                    LispElements p = params[j];

                    if(p instanceof SymbolElement) 
                    {
                    	envir.envsetter(((SymbolElement) p).s, new Element(NIL));
                    }else
                        if(p instanceof Cells && ((Cells) p).element1.getE() instanceof SymbolElement) 
                        {
                        	envir.envsetter(((SymbolElement) ((Cells) p).element1.getE()).s, partMethod(((Cells) ((Cells) p).element2.getE()).element1, envir));
                        }else
                            throw new RuntimeException("// TODO");
                }

                Element a = new Element(NIL);
                for(int j = 1; j < part.length; j++){
                    a = partMethod(contain[j], envir);
                }
                return a;
            }

            if(s.equals("set"))
            {
                if(part.length % 2 != 0)
                    throw new RuntimeException("// TODO");

                Element a = new Element(NIL);
                for(int j = 0; j < part.length; j += 2)
                {
                    LispElements key = partMethod((contain[j]), enviroment).getE();
                    if(!(key instanceof SymbolElement)) 
                    {
                    	 throw new RuntimeException("// TODO");
                    }
                       

                    a = enviroment.envsetter(((SymbolElement)
                    		key).s, partMethod(contain[j + 1], enviroment));
                }

                return a;
            }

            if(s.equals("setq"))
            {
                if(part.length % 2 != 0) 
                {
                	 throw new RuntimeException("// TODO");
                }
                   

                Element a = new Element(NIL);
                for(int j = 0; j < part.length; j += 2)
                {
                    if(!(part[j] instanceof SymbolElement)) 
                    {
                    	 throw new RuntimeException("// TODO");
                    }
                    a = enviroment.envsetter(((SymbolElement) part[j]).s, partMethod(contain[j + 1], enviroment));
                }

                return a;
            }

            if(s.equals("setf"))
            {
                if(part.length % 2 != 0) 
                {
                	   throw new RuntimeException("// TODO"); // TODO fix exception
                }
                 

                Element a = new Element(NIL);
                for(int j = 0; j < part.length; j += 2)
                {
                    Element new_value = partMethod(contain[j + 1], enviroment);

                    if(part[j] instanceof SymbolElement) 
                    {
                    	if(enviroment.stringInEnv(((SymbolElement) part[j]).s)) 
                    	{
                    		(a = enviroment.get(((SymbolElement) part[j]).s)).setE(new_value.getE());
                    	}else
                            enviroment.envsetter(((SymbolElement) part[j]).s,
                            		a = partMethod(contain[j + 1], enviroment));
                    }
                        
                    else{
                        (a = partMethod(contain[j], enviroment)).setE(new_value.getE());
                    }
                }
                return a;
            }

            if(s.equals("lambda"))
            {
                if(part.length != 2 || !(part[0] instanceof Cells)) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception
                }
                    

                Element[] a = ((Cells) part[0]).array();

                for(Element p : a) {
                	 if(!(p.getE() instanceof SymbolElement)) 
                	 {
                		 throw new RuntimeException("// TODO"); // TODO fix exception
                	 }
                         
                }
                return new Element(new LambdaElement((pL) ->
                {
                    Enviroment envi = new Enviroment(enviroment);
                    for(int j = 0; j < a.length; j++)
                    {
                        envi.envsetter(((SymbolElement) a[j].getE()).s,
                        		pL.length > j ? partMethod(pL[j], envi) : new Element(NIL));
                    }
                    return partMethod(contain[1], envi);
                }));
            }

            if(s.equals("defun"))
            {
                if(part.length != 3 || !(part[0] instanceof SymbolElement) || !(part[1] instanceof Cells)) 
                {
                	 throw new RuntimeException("// TODO"); // TODO fix exception
                }
                   

                Element[] a = ((Cells) part[1]).array();

                for(Element p : a)
                    if(!(p.getE() instanceof SymbolElement)) 
                    {
                    	 throw new RuntimeException("// TODO"); // TODO fix exception
                    }
                       

                Element container;

                enviroment.envsetter(((SymbolElement) part[0]).s, container = new Element(new LambdaElement((pL) ->
                {
                    Enviroment envi = new Enviroment(enviroment);
                    for(int j = 0; j < a.length; j++)
                    {
                        envi.envsetter(((SymbolElement) a[j].getE()).s,
                        		pL.length > j ? partMethod(pL[j], envi) : new Element(NIL));
                    }

                    return partMethod(contain[2], envi);
                })));
                return container; 
            }
            if(s.equals("progn"))
            {
                Element a = new Element(NIL);

                for(Element cont2 : contain)
                    a = partMethod(cont2, enviroment);

                return a;
            }

            if(s.equals("partMethod"))
            {
                if(part.length != 1) 
                {
                	 throw new RuntimeException("// TODO"); // TODO fix exception
                }
                   

                return partMethod(partMethod(contain[0], enviroment), enviroment);
            }

            if(s.equals("while"))
            {
                if(part.length != 2) 
                {
                	   throw new RuntimeException("// TODO"); // TODO fix exception
                }
                 
                Element cond = contain[0];
                Element a = new Element(NIL);
              
                while (!partMethod(cond, enviroment).getE().equals(NIL)) 
                {
                	 a = partMethod(contain[1], enviroment);
                }
                 return a;
            }

            if(s.equals("list"))
            {
                Element[] cont5 = new Element[part.length];
                for(int j = 0; j < part.length; j++)
                    cont5[j] = partMethod(contain[j], enviroment);
                return new Element(cont5.length == 0 ? NIL : new Cells(cont5));
            }

            if(s.equals("append"))
            {
                if(part.length < 2) 
                {
                	 throw new RuntimeException("// TODO"); // TODO fix exception
                }
                   

                List<Element> contList = new ArrayList<Element>();

                for(int j = 0; j < part.length; j++)
                {
                    Element contain2 = partMethod(contain[j], enviroment);
                    if(!contain2.getE().equals(NIL)) 
                    {
                    	if(contain2.getE() instanceof Cells) 
                    	{
                    		contList.addAll(Arrays.asList(((Cells) contain2.getE()).array()));
                    	}
                            
                        else
                            if(j != part.length - 1) 
                            {
                            	throw new RuntimeException("// TODO"); // TODO fix exception
                            }else
                                return new Element(new Cells(contList.toArray(new Element[0]), contain2));
                    }
                        
                }
                return new Element(new Cells(contList.toArray(new Element[0])));
            }

            if(hM.containsKey(s))
            {
                if(part.length != 1) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception
                }
                    

                Integer i = hM.get(s);
                Element[] pContainer = ((Cells) partMethod(contain[0], enviroment).getE()).array();

                if(i > pContainer.length - 1)
                {
                	 return new Element(NIL);
                }
                   
                else
                    return pContainer[i];
            }

            if(s.equals("nth") || hM.containsKey(s))
            {
                if(part.length != 2) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception
                }
                    

                if(!(part[0] instanceof Values) || ((Values) part[0]).t.b || ((Values) part[0]).t.compareTo(0) < 0) 
                {
                	 throw new RuntimeException("// TODO"); // TODO fix exception
                }
                   

                Integer i = ((Values) part[0]).t.i.intValue();
                Element[] pContainer = ((Cells) partMethod(contain[1], enviroment).getE()).array();

                if(i > pContainer.length - 1) 
                {
                	  return new Element(NIL);
                }else
                    return pContainer[i];
            }

            if(s.equals("cons"))
            {
                if(part.length != 2) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception	
                }
                    
                return new Element(new Cells(partMethod(contain[0], enviroment), partMethod(contain[1], enviroment)));
            }

            if(s.equals("element1"))
            {
                if(part.length != 1) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception
                }
                    
                Element pContainer = partMethod(contain[0], enviroment);
                if(!(pContainer.getE() instanceof Cells)) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception
                }
                    

                return ((Cells) pContainer.getE()).equals(NIL)
                		? new Element(NIL) : ((Cells) pContainer.getE()).element1;
            }

            if(s.equals("element2"))
            {
                if(part.length != 1) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception
                }
                    
                Element pContainer = partMethod(contain[0], enviroment);
                if(!(pContainer.getE() instanceof Cells)) 
                {
                	throw new RuntimeException("// TODO"); // TODO fix exception	
                }                   
                return ((Cells) pContainer.getE()).equals(NIL) ? new Element(NIL) : ((Cells) pContainer.getE()).element2;
            }

            if(s.startsWith("c") && s.endsWith("r") && s.matches("^c[ad]+r$"))
            {
                if(part.length != 1) 
                {
                	 throw new RuntimeException("// TODO"); // TODO fix exception
                }                   
                Element a = partMethod(contain[0], enviroment);

                for(int j = s.length() - 2; j > 0; j--)
                {
                    if(a.getE().equals(NIL)) 
                    {
                        break;
                    }
                    
                    if(!(a.getE() instanceof Cells)) 
                    {
                    	  throw new RuntimeException("// TODO"); // TODO fix exception
                    }else
                        if(s.charAt(j) == 'a')
                        {
                        	 a = ((Cells) a.getE()).element1;
                        }else

                            a = ((Cells) a.getE()).element2;
                }
                return a;
            }

            if(s.equals("typep"))
            {
                if(part.length != 2 || !(part[1] instanceof SymbolElement)) 
                {
                	 throw new RuntimeException("// TODO"); // TODO fix exception
                }
                   
                LispElements p = partMethod(contain[0], enviroment).getE();

                boolean b = false;
                switch (((SymbolElement) part[1]).s)
                {
                case "null":
                {
                	 b = ((Cells) p).equals(NIL);
                     break;
                }
                   
                case "atom":
                {
                	 b = !(p instanceof Cells) || ((Cells) p).equals(NIL);
                     break;
                }
                   
                case "cons":
                {
                	   b = p instanceof Cells && !((Cells) p).equals(NIL);
                       break;
                }
                 
                case "list":
                {
                	b = p instanceof Cells;
                    break;
                }
                    
                case "number":
                {
                	 b = p instanceof Values;
                     break;
                }
                   
                case "integer":
                {
                	b = p instanceof Values && !((Values) p).isFloat();
                    break;
                }
                    
                case "float":
                {
                	b = p instanceof Values && ((Values) p).isFloat();
                    break;
                }
                    
                case "string":
                {
                	 b = p instanceof StringElement;
                     break;
                }
                   
                case "function":
                {
                	b = p instanceof LambdaElement;
                    break;
                }
                    
                case "symbol":
                {
                	 b = p instanceof SymbolElement;
                     break;
                }
                   
                }

                return new Element(b ? t : NIL);
            }
        }

        Element[] contain = new Element[parts.length];
        for(int j = 0; j < parts.length; j++)
            contain[j] = partMethod(partList2[j], enviroment);

        if(!(contain[0].getE() instanceof LambdaElement)) 
        {
        	throw new RuntimeException("// TODO"); // TODO fix exception
        }Lambda1<Element, Element> function = ((LambdaElement) contain[0].getE()).x;
        Element[] cont2 = Arrays.copyOfRange(contain, 1, contain.length);

   
        return (Element) function.exec(cont2);	 
  
   
    
        
       
        
    }

    private LispElements parse(String str)
    {
        StackForChar stack = new StackForChar(str);
        LispElements a;
        do{
            a = partMethod(stackToPart(stack)).getE();
            stack.clean();
        }
        while (stack.size() != 0);
        return a;
    }
    public String run(String code)
    {
        return parse(code).string();
    }

    public LispElements runAndReturnPart(String code)
    {
        return parse(code);
    }
    
}
