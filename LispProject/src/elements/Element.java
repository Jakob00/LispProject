
package elements;

/**
 * Klassen sparar ett värde som ärver av den abstrakta klassen LispElements
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class Element {
	
	private LispElements e;

    /**
    * Konstruktor
    *
    * @param argE ett objekt av LispElements
    *
    */
    public Element(LispElements argE)
    {
        this.e = argE;
    }

    /**
    * En metod som återger variabeln e
    *
    * @return e som är ett LispElements
    *
    */
    public LispElements getE()
    {
        return e;
    }

    /**
    * En metod som sätter variabeln e
    *
    * @param argE som är ett LispElements
    *
    */
    public void setE(LispElements argE)
    {
        this.e = argE;
    }

    /**
    * En metod som kontrollerar om ett objekt av klassen Object är ekvivalent med variabeln e
    *
    * @param argE som är ett LispElements
    * @return sant eller falsk
    *
    */
    @Override
    public boolean equals(Object o)
    {
        return o!=null&& e!=null&&o instanceof Element&&e.equals(((Element) o).e);
    }

    /**
    * En metod som återger variabeln e som en string
    *
    * @return en String
    *
    */
    public String asString()
    {
        return e.string();
    }

    /**
    * En metod som återger en angiven array av Element som en array av LispElements
    *
    * @param containers en array av element
    * @return en array av LispElements
    *
    */
    public static LispElements[] asParts(Element[] containers)
    {
        LispElements[] result = new LispElements[containers.length];
        for (int i = 0; i < containers.length; i++) 
        {
        	 result[i] = containers[i].getE();	
        }
           

        return result;
    }

}
