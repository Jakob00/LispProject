package elements;

/**
 * Klassen sparar symboler som strängar
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class SymbolElement extends LispElements
{
    public final String s;

    /**
    * Konstruktor 
    *
    * @param argS en string
    *
    */
    public SymbolElement(String argS)
    {
        this.s = argS;
    }

    /**
    * En metod som återger String variabeln s
    *
    * @return en string
    *
    */
    public String string()
    {
        return s;
    }
}
