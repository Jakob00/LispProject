package elements;

/**
 * Klassen sparar ett värde från en klass T
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class Value<T> extends LispElements
{
    
    public final Class<?> typeOfValue;
    public final T t;

    /**
    * Konstruktor
    *
    * @param value av objektet T
    *
    */
    Value(T value)
    {
    	this.typeOfValue=value.getClass();
    	
        this.t = value;
        
    }

    /**
    * En metod som returnerar variabeln t som en string  
    *
    * @return en string
    *
    */
    public String string()
    {
        return t.toString();
    }
}
