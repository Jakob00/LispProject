package elements;


import java.math.RoundingMode;
import java.math.MathContext;
import java.math.BigDecimal;



/**
 * Klassen utför olika matematiska funktioner på en sparad variabel
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class ValueLisp implements Comparable<ValueLisp>
{
    
    public BigDecimal i;
    public boolean b;

    /**
    * Konstruktor med två argument
    *
    * @param j ett objekt av BigDecimal
    * @param c en boolean
    *
    */
    public ValueLisp(BigDecimal j, boolean c)
    {
        this.b = c;
        
        this.i = j;
   
    }

    /**
    * Konstruktor med ett argument 
    *
    * @param s en string
    *
    */
    public ValueLisp(String s)
    {
        try
        {
            i = new BigDecimal(Integer.parseInt(s));
            b = false;
        }
        catch (RuntimeException e)
        {
            try
            {
                i = new BigDecimal(Float.parseFloat(s));
                b = true;
            }
            catch (RuntimeException f)
            {
                try
                {
                    i = new BigDecimal(s);
                    b = true;
                }
                catch (RuntimeException g)
                {
                    throw  new  RuntimeException( s + " error, input has to be a t");
                }
            }
        }
    }

    
    /**
    * En metod för addition
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispPlus(ValueLisp value)
    {
        return new ValueLisp(i.add(roundingValue(value)), b || value.b);
    }

    /**
    * En metod för subtraktion
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispMinus(ValueLisp value)
    {
        return new ValueLisp(i.subtract(roundingValue(value)), b || value.b);
    }

    /**
    * En metod för multiplikation
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispTimes(ValueLisp value)
    {
        return new ValueLisp(i.multiply(roundingValue(value)), b || value.b);
    }

    /**
    * En metod för absolutvärde
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispabs()
    {
        return new ValueLisp(i.abs(), b);
    }

    /**
    * En metod för att hitta maxvärde
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispmax(ValueLisp value)
    {
        return new ValueLisp(i.max(value.i), b || value.b);
    }

    /**
    * En metod för att hitta minvärde
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispmin(ValueLisp value)
    {
        return new ValueLisp(i.min(value.i), b || value.b);
    }

    /**
    * En metod för division
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispObelus(ValueLisp value)
    {
        BigDecimal decimal = i.divide(roundingValue(value));
        boolean newIsFloat = b || value.b;

        if (newIsFloat)
            decimal = decimal.round(new MathContext(MathContext.UNLIMITED.getPrecision(), RoundingMode.DOWN));

        return new ValueLisp(decimal, newIsFloat);
    }

    /**
    * En metod för sinus
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispsin()
    {
        return   new ValueLisp(new BigDecimal(Math.sin(i.doubleValue())), true);
    }

    /**
    * En metod för cosinus
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispcos()
    {
        return new ValueLisp(new BigDecimal(Math.cos(i.doubleValue())), true);
    }

    /**
    * En metod för tangen
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lisptan()
    {
        return new ValueLisp(new BigDecimal(Math.tan(i.doubleValue())), true);
    }

    /**
    * En metod för arcussinus
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispasin()
    {
        return new ValueLisp(new   BigDecimal(Math.asin(i.doubleValue())), true);
    }

    /**
    * En metod för arcuscosinus
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispacos()
    {
        return new ValueLisp(new BigDecimal(Math.acos(i.doubleValue())),   true);
    }

    /**
    * En metod arcustangen
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispatan()
    {
        return new ValueLisp(new BigDecimal(Math.atan(i.doubleValue())), true);
    }

    /**
    * En metod för avrundning upp
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispceil()
    {
        return new ValueLisp(i.round(new MathContext(MathContext.UNLIMITED.getPrecision(),   RoundingMode.CEILING)), true);
    }

    /**
    * En metod för avrundning ned
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispfloor()
    {
        return new ValueLisp(i.round(new MathContext(MathContext.UNLIMITED.getPrecision(), RoundingMode.FLOOR)), true);
    }

    /**
    * En metod för avrundning
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispround()
    {
        return new ValueLisp(i.round(new MathContext(MathContext.UNLIMITED.getPrecision(), RoundingMode.HALF_UP)), true);
    }

    /**
    * En metod för exponentiering
    *
    * @param other ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispexp(ValueLisp other)
    {
        return new ValueLisp(new  BigDecimal(Math.pow(i.doubleValue(), other.i.doubleValue())), true);
    }

    /**
    * En metod för att ta roten ur
    *
    * @return ett objekt av ValueLisp
    *
    */
    public ValueLisp lispsqrt()
    {
        return new ValueLisp(new BigDecimal(Math.sqrt(i.doubleValue())),  true);
    }
    
    /**
    * En metod 
    *
    * @param value ett objekt av ValueLisp
    * @return ett objekt av ValueLisp
    *
    */
    public BigDecimal roundingValue(ValueLisp value)
    {
        if (b)
            return value.i.round(new MathContext(MathContext.UNLIMITED.getPrecision(), RoundingMode.DOWN));
        else
            return value.i;
    }

    /**
    * En metod för att jämföra två tal
    *
    * @param value ett objekt av ValueLisp
    * @return en int
    *
    */
    public int compareTo(ValueLisp value)
    {
        return i.compareTo(value.i);
    }

    /**
    * En metod för att jämföra två tal
    *
    * @param value objekt av Integer
    * @return en int
    *
    */
    public int compareTo(Integer value)
    {
        return  i.compareTo(new BigDecimal(value));
    }

    /**
    * En metod för att jämföra två tal
    *
    * @param value ett objekt av Float
    * @return en int
    *
    */
    public int compareTo(Float value)
    {
        return i.compareTo(new  BigDecimal(value));
    }

    /**
    * En metod för att omvandla variabeln i till en string
    *
    * @return en string
    *
    */
    @Override
    public String toString()
    {
        return i.toString() +  ((b && i.scale() == 0)  ? ".0" : "");
    }
}
