package elements;

import java.math.BigDecimal;

/**
 * Klassen fungerar som klassen Value fast för objekt av ValueLisp
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class Values extends Value<ValueLisp>
{
	/**
    * En metod som kontrollerar om vaiabeln är ett decimal tal
    *
    * @return sant eller falskt
    *
    */
    public boolean isFloat()
    {
        return t.b;
    }

    /**
    * Konstruktor
    *
    * @param arg av ValueLisp
    *
    */
    public Values(ValueLisp arg)
    {
        super(arg);
    }

    /**
    * Konstruktor
    *
    * @param arg av Float
    *
    */
    public Values(Float arg)
    {
        super(new ValueLisp(new BigDecimal(arg), true));
    }
    
    /**
    * Konstruktor
    *
    * @param arg av Integer
    *
    */
    public Values(Integer arg)
    {
        super(new ValueLisp(new BigDecimal(arg), false));
    }

    /**
    * Konstruktor
    *
    * @param arg av Double
    *
    */
    public Values(Double arg)
    {
        super(new ValueLisp(new BigDecimal(arg), true));
    }
}
