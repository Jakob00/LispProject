package elements;

/**
 * Klassen fungerar som klassen Value fast för string
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class StringElement extends Value<String>
{
    /**
    * Konstruktor
    *
    * @param x en String
    *
    */
	public StringElement(String x)
    {
        super(x);
    }
}
