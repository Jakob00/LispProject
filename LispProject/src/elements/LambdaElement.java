package elements;

import lambda.Lambda1;

/**
 * Klassen  
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class LambdaElement extends LispElements
{
    public final Lambda1<Element, Element> x;

    /**
    * Konstruktor
    *
    * @param argX ett objekt som implementerar interfacet Lambda1
    *
    */
    public LambdaElement(Lambda1<Element, Element> argX)
    {
        this.x = argX;
    }
  
    /**
    * En metod som återger "[lambda]"
    *
    * @return en string
    *
    */
    public String string()
    {
        return "[lambda]";
    }
}
