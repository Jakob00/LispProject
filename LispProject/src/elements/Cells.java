package elements;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

import lisp.MyLisp;

/**
 * Klassen Cells består av två variabler av klassen Element
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class Cells extends LispElements
{
    public Element element1;
    public Element element2;
    
    /**
    * Konstruktor
    *
    * @param elements en array av klassen Elements
    * @param arg2 ett objekt av klassen Elements
    *
    */
    public Cells(Element[] elements, Element arg2)
    {
        element1 = elements[0];
        element2 = elements.length>1 ? new Element(new Cells(Arrays.copyOfRange(elements,
        		1,elements.length),arg2)):arg2;
    }
    
    /**
    * Konstruktor
    *
    * @param arg1 ett objekt av klassen Elements
    * @param arg2 ett objekt av klassen Elements
    *
    */
    public Cells(Element arg1, Element arg2)
    {
        this.element1 = arg1;
        
        this.element2 = arg2;
    }

    /**
    * En tom Konstruktor
    *
    */
    public Cells()
    {
    }

    /**
    * Konstruktor
    *
    * @param elements en array av klassen Elements
    *
    */
    public Cells(Element[] elements)
    {
        element1 = elements[0];
        element2 = new Element(elements.length>1 ? 
        		new Cells(Arrays.copyOfRange(elements,1,elements.length)):MyLisp.NIL);
    }

    /**
    * En metod som skapar och återger en array av Cells från variablerna
    *
    * @return en array av klassen Cells
    *
    */
    public Cells[] cellList()
    {
        if(equals(MyLisp.NIL))
            return new Cells[0];

        List<Cells> aL = new ArrayList<Cells>();
        aL.add(this);

        LispElements element = this;

        while ((element = ((Cells) element).element2.getE())instanceof Cells&&!element.equals(MyLisp.NIL))
        {
            aL.add((Cells) element);
        }

        return aL.toArray(new Cells[0]);
        
    }

    /**
    * En metod som skapar och returnerar en array av Element från variablerna 
    *
    * @return en array av klassen Element
    *
    */
    public Element[] array()
    {
        if(equals(MyLisp.NIL)) 
        {
        	return new Element[0];
        }
            

        List<Element> aL = new ArrayList<Element>();
        aL.add(element1);

        if(element2.getE() instanceof Cells) 
        {
        	aL.addAll(Arrays.asList(((Cells) element2.getE()).array()));
        }else {
        	 aL.add(element2);
        }
           

        return aL.toArray(new Element[0]);
    }

    /**
    * En metod som omvandlar och återger en array av Cells till en string
    *
    * @return en string
    *
    */
    public String cellListToString()
    {
        StringBuilder builder = new StringBuilder();

        Cells[] array = cellList();
        
        builder.append("(");

        builder.append(array[0].element1.getE().string());
        
        for(int i=1;i<array.length;i++)
        {
            builder.append(" ");
            builder.append(array[i].element1.getE().string());
        }

        if(!array[array.length - 1].element2.getE().equals(MyLisp.NIL))
        {
            builder.append(" . ");
            builder.append(array[array.length - 1].element2.getE().string());
        }

        builder.append(")");

        return builder.toString();
    }

    /**
    * En metod som returnerar Cells som som string
    *
    * @return en string
    *
    */
    public String cellToString()
    {
        return "(" + element1.getE().string() + " . " + element2.getE().string() + ")";
    }

    /**
    * En metod som kontrollerar om ett objekt av klassen Object är ekvivalent med själva Cells objektet
    *
    * @param other ett objekt av klassen Object
    * @return sant eller falskt
    *
    */
    public boolean equals(Object other)
    {
        if(other==null||!(other instanceof Cells)) 
        {
        	 return false;
        }
           

        if(element1!=null&&((Cells) other).element1!=null&&element1.equals(((Cells) other).element1)) 
        {
        	if(element2 != null && ((Cells)other).element2 != null&& element2.equals(((Cells) other).element2)) 
        	{
        		  return true;
        	}
              
        }return element1 == null && element2 == null && ((Cells) other).element1 == null && ((Cells) other).element2 == null;
    }

    /**
    * En metod som kallar på metoden cellListToString
    *
    * @return en string
    *
    */
    public String string()
    {
        return cellListToString();
    }

    
}
