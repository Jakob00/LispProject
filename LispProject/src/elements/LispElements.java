package elements;

/**
 * En abstrakt klass som ärvs till de andra element-klasserna
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public abstract class LispElements {
	
	/**
 	* En abstrakt metod som returnerar en string 
 	*
 	* @return en String
 	*
 	*/
	public abstract String string();   
}
