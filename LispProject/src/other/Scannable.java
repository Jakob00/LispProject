package other;

import elements.Element;

/**
* Interface som används för att validera en klass giltighet för inmatning
*
* @author Jakob Nygård 19jany01
* @author Joel Johansson 19jojo15
* @version 2020-01-11
*
*/
public interface Scannable
{
    /**
    * En metod som validerar giltighet av inmatning
    *
    * @param x av Element
    * @return sant eller falskt
    *
    */
    boolean scan(Element x);
}
