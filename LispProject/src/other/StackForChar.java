package other;

/**
 * Klassen abstrakt datatyp fungerar som en stack för char
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class StackForChar
{
	
	 private int i;
	 
    public final String s;
   
    /**
    * Konstruktor
    *
    * @param arg en string
    *
    */
    public StackForChar(String arg){
        this.s = arg;
    }

    /**
    * En metod som ångrar en pop
    *
    * @throws RuntimeException
    *
    */
    public void unPop(){
        if(i==0)
            throw new 
            RuntimeException("Stack, error, empty");

        i--;
    }

    /**
    * En metod som poppar stacken
    *
    * @return en char
    * @throws RuntimeException
    *
    */
    public char pop(){
        if(size()==0)
            throw new 
            RuntimeException("Stack, error, empty");

        return s.charAt(i++);
    }

    /**
    * En metod som återger en char på en position i stacken
    *
    * @param pos en int
    * @return en char
    * @throws RuntimeException
    *
    */
    public char peak(int pos){
        if(size()==0)
            throw new 
            RuntimeException("Stack, error, empty");

        if(i+pos<0||i+pos>=s.length())
            throw new 
            RuntimeException("out of bounds");

        return s.charAt(i + pos);
    }
  
    /**
    * En metod som kör peak på position 0
    *
    * @return en char
    *
    */
    public char peak(){
        return peak(0);
    }

    /**
    * En metod som använder pop för att tömma stacken
    *
    * @return sant eller falskt
    * @throws RuntimeException
    *
    */
    public boolean clean(){
        if(size() == 0)
            return false;

       
        char character = peak();
        if(character ==';')
        {
            while (size()!=0&& !noChar2(character = pop()))
            {
            }
            clean();
            return true;
        }
       
        if(noChar(character))
        {
            pop();
            clean();
            return true;
        }
        if(character=='#'&& size()!=0 && peak(1)=='|')
        {
            character = pop();
            character = pop();
            
            while((character = pop())!='#'&& peak(-1)!='|')
            {
                if(size() ==0)
                    throw new RuntimeException("Error comment is not closed");
            }
            character = pop();
            clean();
            return true;
        }

        return false;
    }

    /**
    * En metod återger antal char i stacken
    *
    * @return en int
    *
    */
    public int size(){
        return s.length()-i;
    }

    /**
    * En metod som kontrollerar nästa char
    *
    * @param c en char
    * @return sant eller falskt
    * @throws RuntimeException
    *
    */
    public boolean next(char c){
        if(size()==0)
            throw new 
            RuntimeException("Stack, error, empty");

        return peak()==c;
    }

    /**
    * En metod som kontrollerar ett antal nästkommande char
    *
    * @param s en string
    * @return sant eller falskt
    * @throws RuntimeException
    *
    */
    public boolean next(String s){
        if(size()==0)
            throw new 
            RuntimeException("Stack, error, empty");

        if(size() < s.length())
            return false;

        for(int i = 0; i < s.length(); i++)
            if(s.charAt(i)!=peak(i+1))
                return false;

        return true;
    }

    /**
    * En metod som kontrollerar om nästa char är ett whitespace
    *
    * @return sant eller falskr
    *
    */
    public boolean noCharAtNext(){
        return noChar(peak());
    }

    /**
    * En metod som kontrollerar om en char är ett whitespace
    *
    * @param x en char
    * @return sant eller falskt
    *
    */
    public boolean noChar(char x){
        return x=='\n'||x==' '||x=='\t'||x=='\r';
    }

    /**
    * En metod som kontrollerar om stacken är på sista char
    *
    * @param x en char
    * @return sant eller falskt
    *
    */
    public boolean atLast(char x){
        return  noChar(x)||x==')' ;
    }

    /**
    * En metod som kontrollerar om en char är ett whitespace
    *
    * @param x en char
    * @return sant eller falskt
    *
    */
    public boolean noChar2(char x){
        return x=='\n'||x=='\r';
    }

    


}
