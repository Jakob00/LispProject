package other;

import java.util.HashMap;

import elements.Element;

/**
 * Klassen används för att spara variabler och funktioner, i form av Element, i en HashMap
 *
 * @author Jakob Nygård 19jany01
 * @author Joel Johansson 19jojo15
 * @version 2020-01-11
 *
 */
public class Enviroment
{
   
    protected HashMap<String, Element> hMenv = new HashMap<String, Element>();

    Enviroment envO = null;
    
    /**
    * En tom Konstruktor
    *
    */
    public Enviroment() {    	
    }

    /**
    * Konstruktor
    *
    * @param arg av Environment
    *
    */
    public Enviroment(Enviroment arg)
    {
        this.envO = arg;
    }

    /**
    * En metod som hämtar ett sparat Element med en key
    *
    * @param arg en string
    * @return ett objekt av Element
    * @throws RuntimeException
    *
    */
    public Element get(String arg)
    {
        if (hMenv.containsKey(arg)) 
        {
        	 return hMenv.get(arg);
        }else
            if (envO != null) 
            {
            	return envO.get(arg);
            }else 
            {
            	 throw new 
                 RuntimeException(arg + "= null");
            }
               
    }
    
    /**
    * En metod lägger till ett element i HashMapen med en key
    *
    * @param arg en string
    * @param at av Element
    * @return ett objekt av Element
    *
    */
    public Element envsetter(String arg, Element at)
    {
    	hMenv.put(arg, at);
    	
        return at;
    }

    /**
    * En metod kontrollerar om en key till ett Element finns i HashMapen
    *
    * @param arg en string
    * @return sant eller falskt
    *
    */
    public boolean stringInEnv(String arg)
    {
        return hMenv.containsKey(arg) || (envO != null && envO.stringInEnv(arg));
    }

    

    
}
